
2
Upgrade
Agni Biswas
Be a Git ninja: the .gitattributes file
Pablo Reyes
Pablo Reyes
Follow
May 12, 2018 · 3 min read


Railway merge tracks, like Git
Solve the diff visual problems on your pull request. Or select an automatic action when you have conflicts merging your branchs. For example your images, lock files, etc.
One of the most important file on your repository is .gitignorebut we have also another useful file: .gitattributes.
.gitattribes is used to help Git understand the file contents to better diff/render it. Used for merge resolution strategy (default, union etc.) as well.
Most common uses on .gitattributes
export-ignore

Think, for example, of some PHP library installed via composer. Files are downloaded from Gihub repository, on a zip file.
Why your client need a .gitignore, readme.md or phpunit file? Or the tests/ folder?
Well, you find the utility of this. All files declared with export-ignore will be omitted when repository is downloaded.
# Ignore all test and documentation with “export-ignore”.
.gitattributes export-ignore
.gitignore export-ignore
README.md export-ignore
phpunit.xml.dist export-ignore
/docs export-ignore
/tests export-ignore
text=auto and binary
Handle line endings automatically for files detected as text. Also, all files detected as binary (like an .jpeg file) will be handled untouched. auto perform LF normalization.
You can set files as text or binary type with the filename extension.
* text=auto
*.php text
*.png binary
*.jpg binary
Sometimes, we need to keep files with end of file with CRLF. On these cases, just put eol=crlf. For example, we need conserve for test reasons CRLF on CRLF.php file. Just do
tests/newline/CRLF.php text eol=crl
merge
I’ts possible to set a deffault dessision when conflicts appear: default, ours, then.
# Some developers uses these, I prefer -diff for mistakes prevention
yarn.lock merge=ours
package-lock.json merge=ours
diff
Do not try and merge these files
composer.lock -diff
yarn.lock -diff
public/build/js/*.js -diff
public/build/css/*.css -diff
*.map -diff
rev-manifest.json -diff

Your PR and git diff don’t show differences now. Useful for .lock files.
Previous lines are very helpful when you deal with files like composer.json, packages--lock.json or yarn-lock.json.
linguist-vendored
Remove compiled assets from github statistics. Sometimes we need push files generated, transpiled, minified, etc, and we need them not to be taken into consideration for the repository statistics.
public/build/css/*.css linguist-vendored
public/build/js/* linguist-vendored
public/build/font/* linguist-vendored
Final Words
Of course, you have a lot of more options on official documentation. If you still don’t use this file, it’s time to do it.
If you found this post useful please 👏🏻 and share it!
477

Git
Application Development
Open Source
Continuous Integration
477 claps


Pablo Reyes
WRITTEN BY

Pablo Reyes
Follow
See responses (1)
More From Mediumvdvcsdfgwv;vfv;cxzvdas
vxcvbbfbvbb
cxv
cxvvx
Related reads
Advanced multi-stage build patterns
Tõnis Tiigi
Tõnis Tiigi
Jun 4, 2018 · 6 min read
2.8K

Related reads
How to Use launchd to Run Services in macOS
Kosala Sananthana
Kosala Sananthana in The Startup
May 29 · 5 min read
524

Related reads
Automatic semantic versioning using Gitlab merge request labels
Marc Rooding
Marc Rooding in wbaa
Jan 30 · 6 min read
211

Discover Medium
Welcome to a place where words matter. On Medium, smart voices and original ideas take center stage - with no ads in sight. Watch
Make Medium yours
Follow all the topics you care about, and we’ll deliver the best stories for you to your homepage and inbox. Explore
Become a member
Get unlimited access to the best stories on Medium — and support writers while you’re at it. Just $5/month. Upgrade
About
Help
Legal
